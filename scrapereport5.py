from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from PIL import Image
import re
import datetime
import os

ldec= 50
def getReport(today1, report, shift, isGlobal):
    
    browser.get("https://wolvekrans.south32.max-mine.com")
    shift_name = ''
    
    wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='userId']"))).send_keys('olaf.larson@resolution.systems')
    browser.find_element_by_xpath("//input[@name='password']").send_keys('Baksteen26!')
    browser.find_element_by_xpath("//button[@class='button']").click()
    wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='Date']")))    
    #browser.get('htthttps://wolvekrans.south32.max-mine.com/#/league-tableps://wolvekrans.south32.max-mine.com/#/league-table?Date='+today1[:2]+'%2F'+today1[2:4]+'%2F'+today1[4:]+'&EndDate=undefined&Shift='+shift+'&Crew=&Worker=&rangeSelected=false')
    sleep(1)
    wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='Date']"))).clear()
    wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='Date']"))).send_keys(today1)
    sleep(1)
    wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='Select-control']"))).click()
    sleep(1)
    shiftopt = None
    try:
        shiftopt= browser.find_element_by_xpath("//div[@id='react-select-2--option-"+str(shift)+"']")
    except:
        pass
    
    if shiftopt != None:

        shiftopt.click()
    
        browser.find_element_by_xpath("//span[@id='react-select-3--value']").click() #click on selection 
        wait.until(EC.visibility_of_element_located((By.XPATH,"//div[@class='Select-option']")))
        sleep(2)  

        if not isGlobal :
                  
            while True :
                try:
                    
                    arr = browser.find_elements_by_xpath("//div[contains(text(), '"+report+"') ]")
                    if shift_name == '':
                        shift_name = arr[-1].text.split('Shift')[1][:2].strip()
                    
                    arr[-1].click()
                    browser.find_element_by_xpath("//div[@class='Select-control']").click()
                    sleep(2)
                    browser.find_elements_by_xpath("//div[@class='Select-control']")[1].click()
                    wait.until(EC.visibility_of_element_located((By.XPATH,"//div[@class='Select-option']")))
                    sleep(3)
                                
                    
                except Exception as ee:
                    print (str(ee))
                    break
                
    
    
        browser.find_element_by_xpath("//div[@class='Select-control']").click()
        tabl =  browser.find_element_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[2]/div/table")
        browser.execute_script("return arguments[0].scrollIntoView();", tabl)
        trs = browser.find_elements_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[2]/div/table/tbody/tr")
        
        i= 0
        j=1
        
        
        try:
            browser.execute_script("return arguments[0].scrollIntoView();", trs[i])
            
        except:
            pass
        
            
        tabl =  browser.find_element_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[2]/div/table")
        top = browser.execute_script("return arguments[0].getBoundingClientRect().top;", tabl)
        left = browser.execute_script("return arguments[0].getBoundingClientRect().left;", tabl)
        right = browser.execute_script("return arguments[0].getBoundingClientRect().right;", tabl)
        bottom = browser.execute_script("return arguments[0].getBoundingClientRect().bottom;", tabl)
        #print (top, left, right, bottom)
        
        browser.save_screenshot( 'screenshot0.png')
        
        while (i+7 < len(trs)):
            i+=7
            
            try:
                browser.execute_script("return arguments[0].scrollIntoView();", trs[i])
               
            except Exception as ee:
                trs = browser.find_elements_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[2]/div/table/tbody/tr")
                browser.execute_script("return arguments[0].scrollIntoView();", trs[i])
                print ('exception : ' , str(ee))
            
            browser.save_screenshot('screenshot'+str(j)+'.png')
            j+=1
            
            
        y_offset = 0
        
        print ('scraping first table...')
        
        
        if (j==1):
            
            im = Image.open('screenshot0.png')
            im.thumbnail((1920,1080), Image.ANTIALIAS)
            im = im.crop((left, top, right, bottom))
            table_im = Image.new('RGB', im.size,color=(255,255,255,0))
            table_im.paste(im, (10,10))
            os.remove('screenshot0.png')
            
        else:
            
            for i in range(j):
                im = Image.open('screenshot'+str(i)+'.png')
                im.thumbnail((1920,1080), Image.ANTIALIAS)
                
                if (i == 0):
                    
                    #the blue total = 25
                    im = im.crop((left, top, right, bottom-80))
                    #im.save('2ndscreenshot'+str(i)+'.png') 
                    #table_im = Image.new('RGB', (im.size[0]+20,int(im.size[1]*((2*j-1)/2))),color=(255,255,255,0))
                    table_im = Image.new('RGB', (im.size[0]+20,int(im.size[1]*j)),color=(255,255,255,0))
                    table_im.paste(im, (10,y_offset))
                    y_offset+=im.size[1]
                    
                elif (i==j-1):
                    last_row_num = len(trs)%7
                    if last_row_num == 0:
                        last_row_num = 7
                    
                    
                    
                    im = im.crop((left, bottom-95 -(last_row_num*60), right, bottom-50))
                    #im.save('2ndscreenshot'+str(i)+'.png') 
                    table_im.paste(im, (10,y_offset-10))
                    
                else:
                    
                    im = im.crop((left, top+80, right, bottom-87))
                    #im.save('2ndscreenshot'+str(i)+'.png') 
                    table_im.paste(im, (10,y_offset-10))
                    y_offset+=im.size[1]
               
                os.remove('screenshot'+str(i)+'.png')
           
        table_im.save('table.png')
 
        
#Start with the Tyres & Safety screen
        browser.get("https://wolvekrans.south32.max-mine.com/#/reports")
        sleep(1)
        
        wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='Date']"))).clear()
        
        wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='Date']"))).send_keys(today1)
        
        sleep(2)
        
        wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='Select-control']"))).click()
        
        sleep(4)

        #Select Dayshift of Nightshift
        browser.find_element_by_xpath("//div[@id='react-select-5--option-"+str(shift)+"']").click()
        
        #sleep(2)  
        #Select Crew
        browser.find_element_by_xpath("//span[@id='react-select-6--value']").click() #click on selection  #click on selection
        wait.until(EC.visibility_of_element_located((By.XPATH,"//div[@class='Select-option']")))
        sleep(2)
        
        if not isGlobal:
            
        
            while True :
                try:
                    arr = browser.find_elements_by_xpath("//div[contains(text(), '"+report+"') ]")
                    arr[-1].click()
                    browser.find_element_by_xpath("//div[@class='Select-control']").click()
                    sleep(2)
                    browser.find_elements_by_xpath("//div[@class='Select-control']")[1].click()
                    wait.until(EC.visibility_of_element_located((By.XPATH,"//div[@class='Select-option']")))
                    
                    
                    
                except Exception as ee:
                    print (str(ee))
                    break
                    
        
        wait.until(EC.visibility_of_element_located((By.XPATH,"//*[@id='react-root']/div/div[1]/div/div[2]/div[2]/div/div/div[2]/div/button[1]"))).click()
        wait.until(EC.visibility_of_element_located((By.XPATH,"//*[@id='react-root']/div/div[1]/div/div[2]/div[3]/div/div/div[1]/div[1]")))
            
        map1 =  browser.find_element_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[3]/div/div/div[1]/div[1]")
        browser.execute_script("return arguments[0].scrollIntoView();", map1)
        browser.save_screenshot('mapdiv.png')
        final_im = Image.new('RGB', (1920,1080),color=(255,255,255,0))
        fmapim = Image.open('mapdiv.png')
        fmapim.thumbnail((1920,1080), Image.ANTIALIAS)
        
        maptop = browser.execute_script("return arguments[0].getBoundingClientRect().top;", map1)+45
        mapleft = browser.execute_script("return arguments[0].getBoundingClientRect().left;", map1)
        mapright = browser.execute_script("return arguments[0].getBoundingClientRect().right;", map1)
        mapbottom = browser.execute_script("return arguments[0].getBoundingClientRect().bottom;", map1)-140
        
        tab2 =  browser.find_element_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[3]/div/div/div[1]/div[2]/div/div[1]")
        browser.execute_script("return arguments[0].scrollIntoView();", tab2)
        browser.save_screenshot('tabdiv.png')
        tab2top = browser.execute_script("return arguments[0].getBoundingClientRect().top;", tab2)
        tab2left = browser.execute_script("return arguments[0].getBoundingClientRect().left;", tab2)
        tab2right = browser.execute_script("return arguments[0].getBoundingClientRect().right;", tab2)
        tab2bottom = browser.execute_script("return arguments[0].getBoundingClientRect().bottom;", tab2)
        
        chart =  browser.find_element_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]")
        browser.execute_script("return arguments[0].scrollIntoView();", chart)
        browser.save_screenshot('chartdiv.png')
        charttop = browser.execute_script("return arguments[0].getBoundingClientRect().top;", chart)
        chartleft = browser.execute_script("return arguments[0].getBoundingClientRect().left;", chart)
        chartright = browser.execute_script("return arguments[0].getBoundingClientRect().right;", chart)
        chartbottom = browser.execute_script("return arguments[0].getBoundingClientRect().bottom;", chart)
        
        legend =  browser.find_element_by_xpath("//*[@id='react-root']/div/div[1]/div/div[2]/div[3]/div/div/div[1]/div[1]/div[2]")
        legendtop = browser.execute_script("return arguments[0].getBoundingClientRect().top;", legend)
        legendleft = browser.execute_script("return arguments[0].getBoundingClientRect().left;", legend)
        legendright = browser.execute_script("return arguments[0].getBoundingClientRect().right;", legend)
        legendbottom = browser.execute_script("return arguments[0].getBoundingClientRect().bottom;", legend)
                 
        mapim = fmapim.crop((mapleft, maptop, mapright, mapbottom))
        #mapim.save('mapdiv2.png')
        tabim = Image.open('table.png')
        tabim.thumbnail((900,1080), Image.ANTIALIAS)
        mapim.thumbnail((900,700), Image.ANTIALIAS)
        final_im.paste(tabim, (50,10))
        final_im.paste(mapim, (950,10))
        os.remove('table.png')
        
        print('scraping chart ')
        #chart
        fmapim = Image.open('chartdiv.png')
        fmapim.thumbnail((1920,1080), Image.ANTIALIAS)
        chartim = fmapim.crop((chartleft, charttop, chartright, chartbottom))
        chartim.thumbnail((450,300), Image.ANTIALIAS)
        final_im.paste(chartim, (950,730))
        #legend
        legendim = fmapim.crop((legendleft, legendtop, legendright, legendbottom))
        legendim.thumbnail((900,200), Image.ANTIALIAS)
        final_im.paste(legendim, (950,600))
        os.remove('chartdiv.png')

        print('scraping second table')
        #second table
        fmapim = Image.open('tabdiv.png')
        fmapim.thumbnail((1920,1080), Image.ANTIALIAS)
        tab2im = fmapim.crop((tab2left, tab2top, tab2right, tab2bottom))
        #tab2im.save('tab2im.png')
        tab2im.thumbnail((450,300), Image.ANTIALIAS)
        final_im.paste(tab2im, (1450,730))
        os.remove('tabdiv.png')

        str_today1 = re.sub('/','',today1)
        str_today1 = str_today1[-4:]+str_today1[2:4]+str_today1[0:2]
        
        final_im.save(str_today1+' '+shift+' '+report+'_Shift '+shift_name+'.png')
    else:
        print ('ERROR : No data for the current shift ')    
    
    
  

if __name__ == "__main__":
    
    
    settings={}
    with open('settings.cfg', 'r') as settings_file:
        for line in settings_file:
            (key, val) = line.split(':')
            settings[key.strip()] = val.strip()
    
    
    if settings['DATE'] == '':
        today1 = datetime.datetime.today().strftime('%d/%m/%Y')
    else:
        today1=settings['DATE']
    timeshifts = ['Day', 'Night']
    report_types = ['Boschmanskrans Shovel', 'Boschmanskrans Extraction', 'VDD Box Cut Shovels', 'Steenkoolspruit Extraction']
    options = Options()
    options.add_argument('--disable-browser-side-navigation') 
    options.add_argument("--start-maximized") 
    browser = webdriver.Chrome("chromedriver",chrome_options=options)
    wait = WebDriverWait(browser, 30)
    
    dx, dy = browser.execute_script("var w=window; return [w.outerWidth - w.innerWidth, w.outerHeight - w.innerHeight];")
    browser.set_window_size(1920 + dx, 1080 + dy)
    
    for shift in range(2) :
        for report in report_types:
            
            print ('Report : %s , shift : %s ' %(report, timeshifts[shift]))
            getReport(today1, report, str(shift), False)
    
    for shift in range(2) :
        print ('Report : Global , shift : %s ' %timeshifts[shift])
        getReport(today1, 'Global', str(shift), True)
        
        
        
        
        
try:
    os.remove('mapdiv.png')   
    
except:
    pass
browser.quit()







